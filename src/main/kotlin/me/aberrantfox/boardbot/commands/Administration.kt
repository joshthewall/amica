package me.aberrantfox.boardbot.commands

import me.aberrantfox.boardbot.configuration.MessageLog
import me.aberrantfox.boardbot.configuration.ProjectConfiguration
import me.aberrantfox.kjdautils.api.dsl.CommandSet
import me.aberrantfox.kjdautils.api.dsl.commands
import me.aberrantfox.kjdautils.api.dsl.embed
import me.aberrantfox.kjdautils.internal.command.arguments.*
import me.aberrantfox.kjdautils.internal.di.PersistenceService
import net.dv8tion.jda.core.entities.Category
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.VoiceChannel

@CommandSet("admin")
fun adminCommands(configuration: ProjectConfiguration, persistenceService: PersistenceService) = commands {
    command("interval") {
        description = "Set the interval at which updates happen"
        expect(TimeStringArg)
        execute {
            val time = it.args.first() as Double
            configuration.timerInterval = time.toInt()
            persistenceService.save(configuration)

            it.respond("Set the update interval to $time")
        }
    }

    command("displayConfig") {
        description = "Display the current configuration"
        execute {
            it.respond(configuration.toString())
        }
    }

    command("watch") {
        description = "Mark a category for watching"
        expect(ChannelCategoryArg)
        execute {
            val category = it.args.first() as Category

            if (configuration.notifyCategoryList.contains(category.id)) {
                it.respond("Category is already being watched.")
                return@execute
            }

            configuration.notifyCategoryList.add(category.id)
            persistenceService.save(configuration)
            it.respond("This Category's channels will be watched in the next update.")
        }
    }

    command("ignore") {
        description = "This command will ignore a channel. This means that even if it is in a tracked category it'll never show up."
        expect(TextChannelArg)
        execute {
            val channel = it.args.first() as TextChannel

            if(configuration.ignoredChannels.contains(channel.id)) {
                it.respond("This channel is already ignored.")
                return@execute
            }

            configuration.ignoredChannels.add(channel.id)
            persistenceService.save(configuration)
            it.respond("${channel.name} is now ignored and will no longer show up in updates.")
        }
    }

    val defaultEmbed = embed { title("This message will change into a board in the next interval") }

    command("board") {
        description = "Create a message board in the current channel"
        execute {
            it.channel.sendMessage(defaultEmbed).queue { message ->
                configuration.boardMessages.add(MessageLog(it.channel.id, message.id))
            }
            persistenceService.save(configuration)
        }
    }

    command("trim") {
        description = "This command will trim any dead categories and channels from the configuration file."
        execute {
            val deadIgnoredChannels = configuration.ignoredChannels.filterNot { channel ->
                it.jda.textChannels.any { tc -> tc.id == channel }
            }

            val deadCategories = configuration.notifyCategoryList.filterNot { category ->
                it.jda.categories.any { c -> c.id == category }
            }

            configuration.ignoredChannels.removeAll(deadIgnoredChannels)
            configuration.notifyCategoryList.removeAll(deadCategories)

            it.respond(embed {
                title("Dead configuration items found")
                description("The below items have been found to be dead, and have been removed from the configuration file.")

                field {
                    name = "Ignored Channels"
                    value = deadIgnoredChannels.size.toString()
                }

                field {
                    name = "Dead Categories"
                    value = deadCategories.size.toString()
                }
            })
        }
    }

    command("voicewatch") {
        description = "This command will track any members in a specific voice channel and display them in the noticeboards."
        expect(VoiceChannelArg)
        execute {
            val channel = it.args.first() as VoiceChannel

            if(configuration.notifyVoiceChannels.contains(channel.id)) {
                it.respond("This voice channel is already being watched.")
                return@execute
            }

            configuration.notifyVoiceChannels.add(channel.id)
            persistenceService.save(configuration)
            it.respond("${channel.name} is now a tracked voice channel. Members in the channel will show up in the info boards.")
        }
    }
}