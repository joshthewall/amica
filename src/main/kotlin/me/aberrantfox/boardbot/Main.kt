package me.aberrantfox.boardbot

import me.aberrantfox.kjdautils.api.startBot

fun main(args: Array<String>) {
    if(args.isEmpty()) {
        println("Please provide a token.")
        return
    }

    val token = args.first()

    startBot(token) {
        configure {
            prefix = ";"
            globalPath = "me.aberrantfox.boardbot"
        }
    }
}