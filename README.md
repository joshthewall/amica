## Amica - Friend
Amica is a bot which is explicitly concerned with friendly notifications and information
boards. This bot is made with the idea of having a #info channel in which people can see all
of the server information at a glance. It works particularly well in conjunction with bots 
that create and delete channels. You can also display this information across servers, so if 
you have a lot of servers and want a central place to keep a track of all of them, Amica has 
got your back.

## Command List

| Name | Description | Arguments | Example | 
| -----| ----------- | --------- | ------- | 
| Interval | Set the interval at which updates happen | TimeString | ;;interval 1h |
| DisplayConfig | Display the current configuration | None | ;;displayConfig | 
| watch | Mark a category for watching | ChannelCategoryID | ;;watch 360581491907887104 | 
| ignore | This command will ignore a channel. This means that even if it is in a tracked category it'll never show up. | ChannelID | ;;ignore 244230771232079873 |
| board | Create a message board in the current channel | None | ;;board |
| trim | This command will trim any dead categories and channels from the configuration file. | None | ;;trim |
| voicewatch | This command will track any members in a specific voice channel and display them in the noticeboards. | VoiceChannelID | ;;voicewatch 429614225321754635 |


## Installation

### Windows

Installation Guide WIP

### Linux

#### Abridged Version
1. Install Docker.
2. Clone and `cd` into the top level of the repository.
3. Execute `./scripts/deploy.sh YOUR_BOT_TOKEN ABSOLUTE_PATH_TO_CONFIG_DIRECTORY`
   - `ABSOLUTE_PATH_TO_CONFIG_DIRECTORY` will be used to read + write bot configuration files.
4. Edit `config.json` in the config directory.
5. Re-execute command in step `3`.

#### Longer Version
1. Install Docker, following the appopriate guide for your distro found here: https://docs.docker.com/install/#supported-platforms
2. After Docker is installed, open a terminal and `cd` into a directory you wish to use.
3. Clone the repository with `git clone https://gitlab.com/Aberrantfox/amica.git`
4. `cd` into the newly created directory wtih `cd amica`
5. Execute the command `./scripts/deploy.sh YOUR_BOT_TOKEN ABSOLUTE_PATH_TO_CONFIG_DIRECTORY`
   - Replace `YOUR_BOT_TOKEN` with your Discord bot token.
   - Replace `ABSOLUTE_PATH_TO_CONFIG_DIRECTORY` with the **absolute** file path to the directory where you want the config files to be.
- Example command usage: `./scripts/deploy.sh aokspdf.okwepofk.34p1o32kpo,pqo.sASDAwd /home/me/amica/config`.
1. Edit `config.json`, found in the config directory you chose above.
2. Re-execute the command in step `5`.

### Configuration Overview 
For the most part, the bot can be configured 